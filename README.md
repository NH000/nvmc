Incomplete terminal mail client, originally planned to be implemented with Ncurses.

Requires boost::program_options and vmime to build.

The code was written a long time ago but uploaded in 2022.
