/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_STR_SERVICE_HPP
#define NVMC_STR_SERVICE_HPP

#define NVMC_STR_SERVICE_ERROR_CONNECT(type, ...)		(type == vmime::net::service::Type::TYPE_STORE ? "Store" : "Transport"), " connection error: ", __VA_ARGS__
#define NVMC_STR_SERVICE_ERROR_DISCONNECT(type, ...)	(type == vmime::net::service::Type::TYPE_STORE ? "Store" : "Transport"), " disconnection error: ", __VA_ARGS__

#endif

