/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_LOG_HPP
#define NVMC_LOG_HPP
#include <fstream>
#include <type_traits>

// Use this instead of calling Nvmc::Log::write() directly on the object.
#define NVMC_LOG(...)	Nvmc::Log::instance().write(__PRETTY_FUNCTION__, __VA_ARGS__);

namespace Nvmc
{
	class Log
	{
		Log(const Log&) = delete;
		Log& operator=(const Log&) = delete;
		Log(Log&&) = delete;
		Log& operator=(Log&&) = delete;

		public:
			static Log& instance();

			template <typename T, typename... U, typename = std::enable_if_t<sizeof...(U) != 0>>
			void write(T&& func, U&&... msgs);

		protected:
			explicit Log(const char *filename);

		private:
			std::ofstream file;
	};
}

#include "log.tpp"

#endif
