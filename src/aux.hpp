/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_AUX_HPP
#define NVMC_AUX_HPP
#include <filesystem>
#include <type_traits>
#include <string>
#include <mutex>

// Always use this instead of calling Nvmc::Aux::set_EOF directly.
#define NVMC_SET_EOF(enable)	{										\
									Nvmc::Aux::Lock::set_EOF.lock();	\
									Nvmc::Aux::set_EOF(enable);			\
									Nvmc::Aux::Lock::set_EOF.unlock();	\
								}

namespace Nvmc
{
	namespace Aux
	{
		std::string date_and_time();
		void set_EOF(bool enable);

		std::filesystem::path config_file(bool global);
		std::filesystem::path default_certs_root();
		std::filesystem::path default_certs_user();

		namespace Lock
		{
			extern std::mutex set_EOF;
		}

		namespace Dir
		{
			std::filesystem::path home();

			std::filesystem::path config();
			std::filesystem::path data();
			std::filesystem::path cache();

			std::filesystem::path app_config();
			std::filesystem::path app_data();
			std::filesystem::path app_cache();
		}

		namespace Msg
		{
			template <typename... T, typename = std::enable_if_t<sizeof...(T) != 0>>
			void info(T&&... msgs);
			template <typename... T, typename = std::enable_if_t<sizeof...(T) != 0>>
			void error(T&&... msgs);
			template <typename... T, typename = std::enable_if_t<sizeof...(T) != 0>>
			void warn(T&&... msgs);
		}

		namespace Info
		{
			void version();
			void license();

			template <typename... T>
			void help(T&&... descs);
		}
	}
}

#include "aux.tpp"

#endif
