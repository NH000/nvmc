/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <utility>
#include "session.hpp"
#include "log.hpp"
#include "exception.hpp"
#include "validator/tls_cipher_strength.hpp"
#include "validator/protocol.hpp"
#include "validator/timeout.hpp"
#include "authenticator/plain.hpp"
#include "authenticator/sasl.hpp"
#include "timeout/factory.hpp"
#include "str.hpp"

Nvmc::Session::Session()
	: session(vmime::net::session::create())
{
}

Nvmc::Session::Session(const Options& options)
	: Nvmc::Session::Session()
{
	set_tls_properties(options.get_value<Nvmc::Validator::TLSCipherStrength>("tls.cipher_strength"));

	if (options.get_value<bool>("store.use"))
		activate(vmime::net::service::Type::TYPE_STORE, options);

	if (options.get_value<bool>("transport.use"))
		activate(vmime::net::service::Type::TYPE_TRANSPORT, options);
}

Nvmc::Session::~Session()
{
	NVMC_LOG("Closing session")

	if (is_active(vmime::net::service::Type::TYPE_STORE))
	{
		if (is_connected(vmime::net::service::Type::TYPE_STORE))
		{
			try
			{
				Nvmc::Aux::Msg::info("Disconnecting from the store service...");
				disconnect(vmime::net::service::Type::TYPE_STORE);
				Nvmc::Aux::Msg::info("Disconnection successful");
			}
			catch (const vmime::exceptions::operation_timed_out& error)
			{
				Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_DISCONNECT(vmime::net::service::Type::TYPE_STORE, "Operation timed out"));
				NVMC_LOG(NVMC_STR_SERVICE_ERROR_DISCONNECT(vmime::net::service::Type::TYPE_STORE, "Operation timed out: ", error.what()))
			}
		}

		deactivate(vmime::net::service::Type::TYPE_STORE);
	}

	if (is_active(vmime::net::service::Type::TYPE_TRANSPORT))
	{
		if (is_connected(vmime::net::service::Type::TYPE_TRANSPORT))
		{
			try
			{
				Nvmc::Aux::Msg::info("Disconnecting from the transport service...");
				disconnect(vmime::net::service::Type::TYPE_TRANSPORT);
				Nvmc::Aux::Msg::info("Disconnection successful");
			}
			catch (const vmime::exceptions::operation_timed_out& error)
			{
				Nvmc::Aux::Msg::warn(vmime::net::service::Type::TYPE_TRANSPORT, "Operation timed out");
				NVMC_LOG(vmime::net::service::Type::TYPE_TRANSPORT, "Operation timed out: ", error.what())
			}
		}

		deactivate(vmime::net::service::Type::TYPE_TRANSPORT);
	}

	NVMC_LOG("Closed session")
}

void Nvmc::Session::set_tls_properties(const vmime::net::tls::TLSProperties::GenericCipherSuite strength)
{
	NVMC_LOG("Setting TLS properties")

	auto properties = std::make_shared<vmime::net::tls::TLSProperties>();
	properties->setCipherSuite(strength);
	session->setTLSProperties(properties);

	NVMC_LOG("TLS cipher strength set to ", properties->getCipherSuite())
}

bool Nvmc::Session::is_active(const vmime::net::service::Type type) const noexcept
{
	return type == vmime::net::service::Type::TYPE_STORE ? static_cast<bool>(service_store) : static_cast<bool>(service_transport);
}

bool Nvmc::Session::is_connected(const vmime::net::service::Type type) const
{
	if (!is_active(type))
		throw Nvmc::Exception::Service::Inactive(type);

	return type == vmime::net::service::Type::TYPE_STORE ? service_store->isConnected() : service_transport->isConnected();
}

void Nvmc::Session::activate(const vmime::net::service::Type type, const Options& options)
{
	if (is_active(type))
		throw Nvmc::Exception::Service::Active(type);

	#define NVMC_PROPERTY_STR(session, property, type)	property " = ", session->getProperties().getProperty<type>(property)

	switch (type)
	{
		case vmime::net::service::Type::TYPE_STORE:
		{
			NVMC_LOG("Activating store service")

			std::shared_ptr<vmime::security::authenticator> authenticator;

			{
				const auto& protocol = static_cast<std::string>(options.get_value<Nvmc::Validator::ProtocolStore>("store.protocol"));

				session->getProperties().setProperty("store.protocol", protocol);
				NVMC_LOG("Store protocol set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "store.protocol", std::string))

				if (protocol == "imap")
				{
					session->getProperties().setProperty("store.imap.options.sasl", options.get_value<bool>("store.protocol.imap.sasl"));
					session->getProperties().setProperty("store.imap.options.sasl.fallback", true);
					session->getProperties().setProperty("store.imap.auth.username", options.get_value<std::string>("store.protocol.imap.auth.address"));
					session->getProperties().setProperty("store.imap.auth.password", options.get_value<std::string>("store.protocol.imap.auth.password"));
					session->getProperties().setProperty("store.imap.connection.tls", options.get_value<bool>("store.protocol.imap.tls"));
					session->getProperties().setProperty("store.imap.connection.tls.required", true);
					session->getProperties().setProperty("store.imap.server.address", options.get_value<std::string>("store.protocol.imap.server.address"));
					session->getProperties().setProperty("store.imap.server.port", options.get_value<unsigned int>("store.protocol.imap.server.port"));

					NVMC_LOG("Its properties set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "store.imap.options.sasl", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.options.sasl.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.auth.username", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.auth.password", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.connection.tls", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.connection.tls.required", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.server.address", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imap.server.port", unsigned int))

					authenticator = create_authenticator(session->getProperties().getProperty<bool>("store.imap.options.sasl"), session->getProperties().getProperty<std::string>("store.imap.auth.username"), session->getProperties().getProperty<std::string>("store.imap.auth.password"));
				}
				else if (protocol == "imaps")
				{
					session->getProperties().setProperty("store.imaps.options.sasl", options.get_value<bool>("store.protocol.imaps.sasl"));
					session->getProperties().setProperty("store.imaps.options.sasl.fallback", true);
					session->getProperties().setProperty("store.imaps.auth.username", options.get_value<std::string>("store.protocol.imaps.auth.address"));
					session->getProperties().setProperty("store.imaps.auth.password", options.get_value<std::string>("store.protocol.imaps.auth.password"));
					session->getProperties().setProperty("store.imaps.server.address", options.get_value<std::string>("store.protocol.imaps.server.address"));
					session->getProperties().setProperty("store.imaps.server.port", options.get_value<unsigned int>("store.protocol.imaps.server.port"));

					NVMC_LOG("Its properties set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "store.imaps.options.sasl", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imaps.options.sasl.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imaps.auth.username", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imaps.auth.password", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imaps.server.address", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.imaps.server.port", unsigned int))

					authenticator = create_authenticator(session->getProperties().getProperty<bool>("store.imaps.options.sasl"), session->getProperties().getProperty<std::string>("store.imaps.auth.username"), session->getProperties().getProperty<std::string>("store.imaps.auth.password"));
				}
				else if (protocol == "pop3")
				{
					session->getProperties().setProperty("store.pop3.options.sasl", options.get_value<bool>("store.protocol.pop3.sasl"));
					session->getProperties().setProperty("store.pop3.options.sasl.fallback", true);
					session->getProperties().setProperty("store.pop3.options.apop", options.get_value<bool>("store.protocol.pop3.apop"));
					session->getProperties().setProperty("store.pop3.options.apop.fallback", true);
					session->getProperties().setProperty("store.pop3.auth.username", options.get_value<std::string>("store.protocol.pop3.auth.address"));
					session->getProperties().setProperty("store.pop3.auth.password", options.get_value<std::string>("store.protocol.pop3.auth.password"));
					session->getProperties().setProperty("store.pop3.connection.tls", options.get_value<bool>("store.protocol.pop3.tls"));
					session->getProperties().setProperty("store.pop3.connection.tls.required", true);
					session->getProperties().setProperty("store.pop3.server.address", options.get_value<std::string>("store.protocol.pop3.server.address"));
					session->getProperties().setProperty("store.pop3.server.port", options.get_value<unsigned int>("store.protocol.pop3.server.port"));

					NVMC_LOG("Its properties set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.options.sasl", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.options.sasl.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.options.apop", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.options.apop.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.auth.username", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.auth.password", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.connection.tls", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.connection.tls.required", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.server.address", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3.server.port", unsigned int))

					authenticator = create_authenticator(session->getProperties().getProperty<bool>("store.pop3.options.sasl"), session->getProperties().getProperty<std::string>("store.pop3.auth.username"), session->getProperties().getProperty<std::string>("store.pop3.auth.password"));
				}
				else if (protocol == "pop3s")
				{
					session->getProperties().setProperty("store.pop3s.options.sasl", options.get_value<bool>("store.protocol.pop3s.sasl"));
					session->getProperties().setProperty("store.pop3s.options.sasl.fallback", true);
					session->getProperties().setProperty("store.pop3s.options.apop", options.get_value<bool>("store.protocol.pop3s.apop"));
					session->getProperties().setProperty("store.pop3s.options.apop.fallback", true);
					session->getProperties().setProperty("store.pop3s.auth.username", options.get_value<std::string>("store.protocol.pop3s.auth.address"));
					session->getProperties().setProperty("store.pop3s.auth.password", options.get_value<std::string>("store.protocol.pop3s.auth.password"));
					session->getProperties().setProperty("store.pop3s.server.address", options.get_value<std::string>("store.protocol.pop3s.server.address"));
					session->getProperties().setProperty("store.pop3s.server.port", options.get_value<unsigned int>("store.protocol.pop3s.server.port"));

					NVMC_LOG("Its properties set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.options.sasl", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.options.sasl.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.options.apop", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.options.apop.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.auth.username", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.auth.password", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.server.address", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "store.pop3s.server.port", unsigned int))

					authenticator = create_authenticator(session->getProperties().getProperty<bool>("store.pop3s.options.sasl"), session->getProperties().getProperty<std::string>("store.pop3s.auth.username"), session->getProperties().getProperty<std::string>("store.pop3s.auth.password"));
				}
				else if (protocol == "maildir")
				{
					session->getProperties().setProperty("store.maildir.server.rootpath", static_cast<std::string>(options.get_value<std::filesystem::path>("store.protocol.maildir.folder")));

					NVMC_LOG("Its properties set to:\n",
							'\t', NVMC_PROPERTY_STR(session, "store.maildir.server.rootpath", std::string))
				}
			}

			NVMC_LOG("Getting store service")
			service_store = session->getStore(std::move(authenticator));

			NVMC_LOG("Setting timeout for the service operations")
			service_store->setTimeoutHandlerFactory(std::make_shared<Nvmc::Timeout::Factory>(options.get_value<Nvmc::Validator::Timeout>("store.timeout")));
		}
		break;
		case vmime::net::service::Type::TYPE_TRANSPORT:
		{
			NVMC_LOG("Activating transport service")

			std::shared_ptr<vmime::security::authenticator> authenticator;

			{
				const auto& protocol = static_cast<std::string>(options.get_value<Nvmc::Validator::ProtocolTransport>("transport.protocol"));

				session->getProperties().setProperty("transport.protocol", protocol);
				NVMC_LOG("Transport protocol set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "transport.protocol", std::string))

				if (protocol == "smtp")
				{
					session->getProperties().setProperty("transport.smtp.options.sasl", options.get_value<bool>("transport.protocol.smtp.sasl"));
					session->getProperties().setProperty("transport.smtp.options.sasl.fallback", true);
					session->getProperties().setProperty("transport.smtp.auth.username", options.get_value<std::string>("transport.protocol.smtp.auth.address"));
					session->getProperties().setProperty("transport.smtp.auth.password", options.get_value<std::string>("transport.protocol.smtp.auth.password"));
					session->getProperties().setProperty("transport.smtp.connection.tls", options.get_value<bool>("transport.protocol.smtp.tls"));
					session->getProperties().setProperty("transport.smtp.connection.tls.required", true);
					session->getProperties().setProperty("transport.smtp.server.address", options.get_value<std::string>("transport.protocol.smtp.server.address"));
					session->getProperties().setProperty("transport.smtp.server.port", options.get_value<unsigned int>("transport.protocol.smtp.server.port"));
					session->getProperties().setProperty("transport.smtp.options.need-authentication", options.get_value<bool>("transport.protocol.smtp.server.authentication"));
					session->getProperties().setProperty("transport.smtp.options.pipelining", options.get_value<bool>("transport.protocol.smtp.server.pipelining"));
					session->getProperties().setProperty("transport.smtp.options.chunking", options.get_value<bool>("transport.protocol.smtp.server.chunking"));

					NVMC_LOG("Its properties set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.options.sasl", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.options.sasl.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.auth.username", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.auth.password", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.connection.tls", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.connection.tls.required", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.server.address", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.server.port", unsigned int), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.options.need-authentication", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.options.pipelining", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtp.options.chunking", bool))

					authenticator = create_authenticator(session->getProperties().getProperty<bool>("transport.smtp.options.sasl"), session->getProperties().getProperty<std::string>("transport.smtp.auth.username"), session->getProperties().getProperty<std::string>("transport.smtp.auth.password"));
				}
				else if (protocol == "smtps")
				{
					session->getProperties().setProperty("transport.smtps.options.sasl", options.get_value<bool>("transport.protocol.smtps.sasl"));
					session->getProperties().setProperty("transport.smtps.options.sasl.fallback", true);
					session->getProperties().setProperty("transport.smtps.auth.username", options.get_value<std::string>("transport.protocol.smtps.auth.address"));
					session->getProperties().setProperty("transport.smtps.auth.password", options.get_value<std::string>("transport.protocol.smtps.auth.password"));
					session->getProperties().setProperty("transport.smtps.server.address", options.get_value<std::string>("transport.protocol.smtps.server.address"));
					session->getProperties().setProperty("transport.smtps.server.port", options.get_value<unsigned int>("transport.protocol.smtps.server.port"));
					session->getProperties().setProperty("transport.smtps.options.need-authentication", options.get_value<bool>("transport.protocol.smtps.server.authentication"));
					session->getProperties().setProperty("transport.smtps.options.pipelining", options.get_value<bool>("transport.protocol.smtps.server.pipelining"));
					session->getProperties().setProperty("transport.smtps.options.chunking", options.get_value<bool>("transport.protocol.smtps.server.chunking"));

					NVMC_LOG("Its properties set to:\n",
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.options.sasl", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.options.sasl.fallback", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.auth.username", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.auth.password", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.server.address", std::string), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.server.port", unsigned int), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.options.need-authentication", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.options.pipelining", bool), '\n',
						'\t', NVMC_PROPERTY_STR(session, "transport.smtps.options.chunking", bool))

					authenticator = create_authenticator(session->getProperties().getProperty<bool>("transport.smtps.options.sasl"), session->getProperties().getProperty<std::string>("transport.smtps.auth.username"), session->getProperties().getProperty<std::string>("transport.smtps.auth.password"));
				}
				else if (protocol == "sendmail")
				{
					session->getProperties().setProperty("transport.sendmail.binpath", static_cast<std::string>(options.get_value<std::filesystem::path>("transport.protocol.sendmail.bin")));

					NVMC_LOG("Its properties set to:\n",
							'\t', NVMC_PROPERTY_STR(session, "transport.sendmail.binpath", std::string))
				}
			}

			NVMC_LOG("Getting transport service")
			service_transport = session->getTransport(std::move(authenticator));

			NVMC_LOG("Setting timeout for the service operations")
			service_transport->setTimeoutHandlerFactory(std::make_shared<Nvmc::Timeout::Factory>(options.get_value<Nvmc::Validator::Timeout>("transport.timeout")));
		}
		break;
	}

	#undef NVMC_PROPERTY_STR
}

void Nvmc::Session::deactivate(const vmime::net::service::Type type)
{
	if (!is_active(type))
		throw Nvmc::Exception::Service::Inactive(type);

	switch (type)
	{
		case vmime::net::service::Type::TYPE_STORE:
			NVMC_LOG("Deactivating store service")
			service_store.reset();
			break;
		case vmime::net::service::Type::TYPE_TRANSPORT:
			NVMC_LOG("Deactivating transport service")
			service_transport.reset();
			break;
	}
}

void Nvmc::Session::connect(const vmime::net::service::Type type)
{
	if (is_connected(type))
		throw Nvmc::Exception::Service::Connected(type);

	switch (type)
	{
		case vmime::net::service::Type::TYPE_STORE:
			NVMC_LOG("Connecting to the store service")
			service_store->connect();
			break;
		case vmime::net::service::Type::TYPE_TRANSPORT:
			NVMC_LOG("Connecting to the transport service")
			service_transport->connect();
			break;
	}

	NVMC_LOG("Connection successful")
}

void Nvmc::Session::disconnect(const vmime::net::service::Type type)
{
	if (!is_connected(type))
		throw Nvmc::Exception::Service::Disconnected(type);

	switch (type)
	{
		case vmime::net::service::Type::TYPE_STORE:
			NVMC_LOG("Disconnecting from the store service")
			service_store->disconnect();
			break;
		case vmime::net::service::Type::TYPE_TRANSPORT:
			NVMC_LOG("Disconnecting from the transport service")
			service_transport->disconnect();
			break;
	}

	NVMC_LOG("Disconnection successful")
}

inline std::shared_ptr<vmime::security::authenticator> Nvmc::Session::create_authenticator(bool sasl, const std::string& username, const std::string& password) const
{
	if (sasl)
	{
		NVMC_LOG("Creating SASL authenticator")
		return std::make_shared<Nvmc::Authenticator::SASL>(username, password);
	}

	NVMC_LOG("Creating plain authenticator")
	return std::make_shared<Nvmc::Authenticator::Plain>(username, password);
}
