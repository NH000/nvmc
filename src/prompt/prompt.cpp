#include <iostream>
#include "prompt.hpp"
#include "../aux.hpp"

void Nvmc::Prompt::Prompt::add_answer(const std::string& answer, const std::optional<std::string>& alt_answer)
{
	if (const auto&& ref = answers.insert(answer).first;alt_answer)
		alt_answers[*alt_answer] = &(*ref);
}

const std::string& Nvmc::Prompt::Prompt::run() const
{
	const bool free_input = answers.empty();

	NVMC_SET_EOF(false);

	while (true)
	{
		std::cout << question << ' ';

		if (!free_input)
		{
			std::cout << '[';

			for (auto&& answer = answers.begin();answer != answers.end();++answer)
			{
				std::cout << *answer;
				std::cout << (std::next(answer) == answers.end() ? ']' : '/');
			}

			std::cout << ' ';
		}

		static std::string response;
		std::getline(std::cin, response);

		if (free_input)
		{
			NVMC_SET_EOF(true);
			return response;
		}
		else
		{
			if (const auto& answer = answers.find(response);answer != answers.end())
			{
				NVMC_SET_EOF(true);
				return *answer;
			}
			else if (const auto& answer = alt_answers.find(response);answer != alt_answers.end())
			{
				NVMC_SET_EOF(true);
				return *((*answer).second);
			}
		}
	}
}

void Nvmc::Prompt::Prompt::clear()
{
	question.clear();
	answers.clear();
	alt_answers.clear();
}
