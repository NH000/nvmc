/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_PROMPT_HPP
#define NVMC_PROMPT_HPP
#include <string>
#include <optional>
#include <set>
#include <unordered_map>

namespace Nvmc
{
	namespace Prompt
	{
		class Prompt
		{
			friend class Factory;

			public:
				virtual ~Prompt() = default;

				template <typename... T>
				void set_question(T&&... parts);

				void add_answer(const std::string& answer, const std::optional<std::string>& alt_answer = std::nullopt);
				virtual const std::string& run() const;
				void clear();

			protected:
				explicit Prompt() = default;

				std::string question;
				std::set<std::string> answers;
				std::unordered_map<std::string, const std::string*> alt_answers;
		};
	}
}

#include "prompt.tpp"

#endif
