/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_PROMPT_FACTORY_HPP
#define NVMC_PROMPT_FACTORY_HPP
#include <memory>
#include <string>
#include "prompt.hpp"

namespace Nvmc
{
	namespace Prompt
	{
		class Factory final
		{
			Factory() = delete;

			public:
				static std::shared_ptr<Nvmc::Prompt::Prompt> create();
				static void set_prompt_type(const std::string& type);

			private:
				static std::string type;
		};
	}
}

#endif
