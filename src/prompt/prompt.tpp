template <typename... T>
void Nvmc::Prompt::Prompt::set_question(T&&... parts)
{
	question.clear();
	question = (question + ... + std::forward<T>(parts));
}
