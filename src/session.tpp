#include <utility>
#include "exception.hpp"

template <typename T>
void Nvmc::Session::set_certificate_verifier(const vmime::net::service::Type type, T&& verifier)
{
	if (!is_active(type))
		throw Nvmc::Exception::Service::Inactive(type);

	(type == vmime::net::service::Type::TYPE_STORE ? static_cast<std::shared_ptr<vmime::net::service>>(service_store) : static_cast<std::shared_ptr<vmime::net::service>>(service_transport))->setCertificateVerifier(std::forward<T>(verifier));
}
