/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <memory>
#include <cstdlib>
#include "options.hpp"
#include "aux.hpp"
#include "success_code.hpp"
#include "log.hpp"
#include "info.hpp"
#include "session.hpp"
#include "cert/verifier/interactive.hpp"
#include "str.hpp"

#define NVMC_SERVICE_CONNECT(session, type)	try																																		\
											{																																		\
												Nvmc::Aux::Msg::info("Connecting to the ", type == vmime::net::service::Type::TYPE_STORE ? "store" : "transport", " service...");	\
												session->connect(type);																												\
												Nvmc::Aux::Msg::info("Connection successful");																						\
											}																																		\
											catch (const vmime::exceptions::authentication_error& error)																			\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Failed to authenticate to the server"));								\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Failed to authenticate to the server: ", error.what()));								\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::exceptions::no_auth_information& error)																				\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Authentication information not supplied"));							\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Authentication information not supplied: ", error.what()));							\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::exceptions::sasl_exception& error)																					\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "SASL authentication method failed"));									\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "SASL authentication method failed: ", error.what()));								\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::exceptions::tls_exception& error)																					\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Unable to establish connection over TLS"));							\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Unable to establish connection over TLS: ", error.what()));							\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::exceptions::connection_error& error)																				\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Connection error"));													\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Connection error: ", error.what()));													\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::exceptions::connection_greeting_error& error)																		\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Connection greeting error"));											\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Connection greeting error: ", error.what()));										\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::security::cert::certificateException& error)																		\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Certificate verification failure"));									\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Certificate verification failure: ", error.what()));									\
												session->deactivate(type);																											\
											}																																		\
											catch (const vmime::exceptions::operation_timed_out& error)																				\
											{																																		\
												Nvmc::Aux::Msg::warn(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Operation timed out"));												\
												NVMC_LOG(NVMC_STR_SERVICE_ERROR_CONNECT(type, "Operation timed out: ", error.what()));												\
												session->deactivate(type);																											\
											}

int main(int argc, char *argv[])
{
	NVMC_LOG("Starting ", NVMC_INFO_NAME)

	std::unique_ptr<Nvmc::Options> options(std::make_unique<Nvmc::Options>());

	try
	{
		options->parse_args(argc, argv);
	}
	catch (const boost::program_options::error& error)
	{
		NVMC_LOG("Error while parsing command-line: ", error.what())
		Nvmc::Aux::Msg::error(error.what());
		NVMC_LOG(NVMC_EXIT_MSG(Nvmc::SuccessCode::Type::INVALID_OPT))
		return Nvmc::SuccessCode::Type::INVALID_OPT;
	}

	if (options->get_value<bool>("help"))
	{
		NVMC_LOG("Displaying help menu to the user")
		Nvmc::Aux::Info::help(options->get_desc_generic());
		NVMC_LOG(NVMC_EXIT_MSG(Nvmc::SuccessCode::Type::SUCCESS))
		return Nvmc::SuccessCode::Type::SUCCESS;
	}
	if (options->get_value<bool>("version"))
	{
		NVMC_LOG("Displaying version to the user")
		Nvmc::Aux::Info::version();
		NVMC_LOG(NVMC_EXIT_MSG(Nvmc::SuccessCode::Type::SUCCESS))
		return Nvmc::SuccessCode::Type::SUCCESS;
	}
	if (options->get_value<bool>("license"))
	{
		NVMC_LOG("Displaying license to the user")
		Nvmc::Aux::Info::license();
		NVMC_LOG(NVMC_EXIT_MSG(Nvmc::SuccessCode::Type::SUCCESS))
		return Nvmc::SuccessCode::Type::SUCCESS;
	}

	try
	{
		options->parse_config(Nvmc::Aux::config_file(false));
		if (!getenv("NVMC_DO_NOT_PARSE_GLOBAL_CONFIG"))
			options->parse_config(Nvmc::Aux::config_file(true));
		options->parse_env();
		options->parse_done();
	}
	catch (const boost::program_options::error& error)
	{
		NVMC_LOG("Error while parsing configuration file and environment: ", error.what())
		Nvmc::Aux::Msg::error(error.what());
		NVMC_LOG(NVMC_EXIT_MSG(Nvmc::SuccessCode::Type::INVALID_OPT))
		return Nvmc::SuccessCode::Type::INVALID_OPT;
	}

	NVMC_LOG("Opening session")
	std::unique_ptr<Nvmc::Session> session(std::make_unique<Nvmc::Session>(*options));

	{
		std::shared_ptr<Nvmc::Cert::Verifier::Interactive> verifier(std::make_shared<Nvmc::Cert::Verifier::Interactive>(options->get_value<std::filesystem::path>("tls.certs.root"), options->get_value<std::filesystem::path>("tls.certs.user")));

		if (session->is_active(vmime::net::service::Type::TYPE_STORE))
		{
			session->set_certificate_verifier(vmime::net::service::Type::TYPE_STORE, verifier);
			NVMC_SERVICE_CONNECT(session, vmime::net::service::Type::TYPE_STORE);
		}

		if (session->is_active(vmime::net::service::Type::TYPE_TRANSPORT))
		{
			session->set_certificate_verifier(vmime::net::service::Type::TYPE_TRANSPORT, std::move(verifier));
			NVMC_SERVICE_CONNECT(session, vmime::net::service::Type::TYPE_TRANSPORT);
		}
	}

	NVMC_LOG(NVMC_EXIT_MSG(Nvmc::SuccessCode::Type::SUCCESS))
	return Nvmc::SuccessCode::Type::SUCCESS;
}
