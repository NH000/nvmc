/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_EXCEPTION_HPP
#define NVMC_EXCEPTION_HPP
#include <vmime/vmime.hpp>
#include <exception>
#include <filesystem>

namespace Nvmc
{
	namespace Exception
	{
		class DateAndTimeFailure : public std::exception
		{
			public:
				const char *what() const noexcept override;
		};

		class HomeUnavailable : public std::exception
		{
			public:
				const char *what() const noexcept override;
		};

		class CorruptedCmdArgs : public std::exception
		{
			public:
				const char *what() const noexcept override;
		};

		namespace Filesystem
		{
			class FailureOpen : std::filesystem::filesystem_error
			{
				public:
					explicit FailureOpen(const std::filesystem::path& filename, bool read, bool write, const std::error_code& error_code);
			};
		}

		namespace Service
		{
			class Active : public std::exception
			{
				public:
					explicit Active(vmime::net::service::Type type);
					const char *what() const noexcept override;

				protected:
					std::string msg;
			};

			class Inactive : public std::exception
			{
				public:
					explicit Inactive(vmime::net::service::Type type);
					const char *what() const noexcept override;

				protected:
					std::string msg;
			};

			class Connected : public std::exception
			{
				public:
					explicit Connected(vmime::net::service::Type type);
					const char *what() const noexcept override;

				protected:
					std::string msg;
			};

			class Disconnected : public std::exception
			{
				public:
					explicit Disconnected(vmime::net::service::Type type);
					const char *what() const noexcept override;

				protected:
					std::string msg;
			};
		}
	}
}

#endif
