/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_OPTIONS_HPP
#define NVMC_OPTIONS_HPP
#include <filesystem>
#include <string>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>

namespace Nvmc
{
	class Options
	{
		public:
			explicit Options();

			void parse_args(const int& argc, const char * const *argv);
			void parse_config(const std::filesystem::path& filename);
			void parse_env();
			void parse_done();

			const boost::program_options::options_description& get_desc_generic() const;
			const boost::program_options::options_description& get_desc_config() const;
			const boost::program_options::options_description& get_desc_env() const;

			template <typename T>
			const T& get_value(const std::string& name) const;

		protected:
			boost::program_options::variables_map option_map;

		private:
			void init_desc_generic() const;
			void init_desc_config() const;
			void init_desc_env() const;

			mutable boost::program_options::options_description desc_generic;
			mutable boost::program_options::options_description desc_config;
			mutable boost::program_options::options_description desc_env;
	};

	template <>
	const bool& Options::get_value<bool>(const std::string& name) const;
}

#include "options.tpp"

#endif
