#include <type_traits>
#include "basic.hpp"
#include "../../exception.hpp"

const std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>& Nvmc::Cert::Loader::Basic::get() const
{
	return certs;
}

std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>& Nvmc::Cert::Loader::Basic::get()
{
	return const_cast<std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>&>(std::as_const(*this).get());
}

std::shared_ptr<vmime::security::cert::X509Certificate> Nvmc::Cert::Loader::Basic::load(const std::filesystem::path& filename) const
{
	std::ifstream file;
	file.exceptions(std::ios_base::badbit);

	try
	{
		file.open(filename, std::ios_base::binary);
	}
	catch (const std::ios_base::failure& error)
	{
		throw Nvmc::Exception::Filesystem::FailureOpen(filename, true, false, error.code());
	}

	return load(file);
}

std::shared_ptr<vmime::security::cert::X509Certificate> Nvmc::Cert::Loader::Basic::load(std::ifstream& file) const
{
	vmime::utility::inputStreamAdapter isa(file);
	return vmime::security::cert::X509Certificate::import(isa);
}
