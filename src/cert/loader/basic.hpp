#ifndef NVMC_CERT_LOADER_BASIC_HPP
#define NVMC_CERT_LOADER_BASIC_HPP
#include <vmime/vmime.hpp>
#include <filesystem>
#include <vector>
#include <fstream>

namespace Nvmc
{
	namespace Cert
	{
		namespace Loader
		{
			class Basic
			{
				public:
					explicit Basic() = default;

					template <typename T>
					void add(T&& data);

					const std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>& get() const;
					std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>& get();

				protected:
					std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>> certs;

				private:
					std::shared_ptr<vmime::security::cert::X509Certificate> load(const std::filesystem::path& filename) const;
					std::shared_ptr<vmime::security::cert::X509Certificate> load(std::ifstream& file) const;
			};
		}
	}
}

#include "basic.tpp"

#endif
