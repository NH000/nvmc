#include <utility>

template <typename T>
void Nvmc::Cert::Loader::Basic::add(T&& data)
{
	certs.push_back(load(std::forward<T>(data)));
}
