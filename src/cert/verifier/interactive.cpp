#include <utility>
#include <type_traits>
#include <fstream>
#include "interactive.hpp"
#include "../loader/basic.hpp"
#include "../../prompt/factory.hpp"
#include "../../log.hpp"
#include "../../aux.hpp"
#include "../../exception.hpp"

Nvmc::Cert::Verifier::Interactive::Interactive(std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>&& certs_root, std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>&& certs_user)
	: certs_root(std::move(certs_root)),
	certs_user(std::move(certs_user))
{
}

Nvmc::Cert::Verifier::Interactive::Interactive(const std::filesystem::path& certs_root_dir, const std::filesystem::path& certs_user_dir)
	: certs_root_dir(certs_root_dir),
	certs_user_dir(certs_user_dir),
	certs_root(std::move(load_certificates(certs_root_dir))),
	certs_user(std::move(load_certificates(certs_user_dir)))
{
}

void Nvmc::Cert::Verifier::Interactive::verify(const std::shared_ptr<vmime::security::cert::certificateChain>& chain, const std::string& hostname)
{
	if (chain->getCount() == 0)
	{
		NVMC_LOG("Server ", hostname, " did not send certificate chain")
		return;
	}

	Nvmc::Aux::Msg::info("Verifying certificate chain from ", hostname, "...");
	NVMC_LOG("Verifying certificate chain from ", hostname, "...")

	const auto& type = chain->getAt(0)->getType();

	NVMC_LOG("Certificates are of type: ", type)

	if (type == "X.509")
		verifyX509(chain, hostname);
	else
	{
		NVMC_LOG("Unsupported certificate type")
		throw vmime::security::cert::unsupportedCertificateTypeException(type);
	}
}

void Nvmc::Cert::Verifier::Interactive::verifyX509(const std::shared_ptr<vmime::security::cert::certificateChain>& chain, const std::string& hostname)
{
	const auto chain_count = chain->getCount();

	NVMC_LOG("Checking whether each certificate in the chain is issued by the next one")
	for (std::remove_const_t<decltype(chain_count)> i = 0;i < chain_count - 1;++i)
	{
		if (const auto& cert = vmime::dynamicCast<vmime::security::cert::X509Certificate>(chain->getAt(i));!cert->checkIssuer(vmime::dynamicCast<vmime::security::cert::X509Certificate>(chain->getAt(i + 1))))
		{
			vmime::security::cert::certificateIssuerVerificationException error;
			error.setCertificate(cert);
			throw error;
		}
	}

	NVMC_LOG("Making sure that each certificate is valid at the current time")
	for (std::remove_const_t<decltype(chain_count)> i = 0;i < chain_count;++i)
		vmime::dynamicCast<vmime::security::cert::X509Certificate>(chain->getAt(i))->checkValidity();

	NVMC_LOG("Testing whether the subject's name matches the hostname")
	const auto& first = vmime::dynamicCast<vmime::security::cert::X509Certificate>(chain->getAt(0));
	if (!first->verifyHostName(hostname))
	{
		vmime::security::cert::serverIdentityException error;
		error.setCertificate(first);
		throw error;
	}

	NVMC_LOG("Ensuring that the certificate chain can be trusted")
	{
		bool trusted = false;

		{
			NVMC_LOG("Check whether the last certificate is issued by a third-party that we trust")

			const auto& last = vmime::dynamicCast<vmime::security::cert::X509Certificate>(chain->getAt(chain_count - 1));
			for (const auto& cert : certs_root)
			{
				if (last->verify(cert))
				{
					NVMC_LOG("Success: Last certificate is issued by a third-party that we trust")
					trusted = true;
					break;
				}
			}
		}

		if (!trusted)
		{
			NVMC_LOG("Failure: Last certificate is not issued by a third-party that we trust")
			NVMC_LOG("Alternatively, test whether the subject is among our trusted certificates")

			for (const auto& cert : certs_user)
			{
				if (first->equals(cert))
				{
					NVMC_LOG("Success: Subject is among our trusted certificates")
					trusted = true;
					break;
				}
			}
		}

		if (!trusted)
		{
			NVMC_LOG("Failure: Subject is not among our trusted certificates")

			NVMC_LOG("Checking whether the subject's certificate was already rejected before asking the user to explicitly choose to trust it")
			for (const auto& cert : rejected)
			{
				if (cert->equals(first))
				{
					NVMC_LOG("Subject's certificate was already rejected, stopping")

					vmime::security::cert::certificateException error;
					error.setCertificate(first);
					throw error;
				}
			}
			NVMC_LOG("Certificate was not previously rejected")

			NVMC_LOG("Asking the user whether the subject should be trusted")
			auto prompt = Nvmc::Prompt::Factory::create();
			prompt->set_question("Failed to verify the subject's certificate from ", hostname, ". Do you want to trust it anyway?");
			prompt->add_answer("yes", "y");
			prompt->add_answer("no", "n");
			auto response = prompt->run();

			NVMC_LOG("User answered: ", response)

			if (response == "yes")
			{
				certs_user.push_back(first);
				NVMC_LOG("Added subject's certificate to the list of trusted certificates")

				NVMC_LOG("Asking the user whether to save the subject's certificate for later use")
				prompt->set_question("Do you want to save the subject's certificate to the directory of trusted certificates for later use?");
				response = prompt->run();

				NVMC_LOG("User answered: ", response)

				if (response == "yes")
				{
					NVMC_LOG("Querying user for the format in which to save the subject's certificate")
					prompt->clear();
					prompt->set_question("Choose format:");
					prompt->add_answer("der");
					prompt->add_answer("pem");
					response = prompt->run();

					NVMC_LOG("User chose: ", response)

					try
					{
						std::filesystem::create_directories(certs_user_dir);
					}
					catch (const std::filesystem::filesystem_error& error)
					{
						Nvmc::Aux::Msg::warn("Failed to create directory for user trusted certificates: ", certs_user_dir, ", certificate not saved");
						NVMC_LOG("Failed to create directory for user trusted certificates: ", certs_user_dir, "certificate not saved: ", error.what())
						return;
					}

					const auto save_name = certs_user_dir / (hostname + '.' + response);
					std::ofstream save_file;
					save_file.exceptions(std::ios_base::badbit);

					try
					{
						save_file.open(save_name, std::ios_base::trunc | std::ios_base::binary);
					}
					catch (const std::ios_base::failure& error)
					{
						Nvmc::Aux::Msg::warn("Failed to open ", save_name, " for writing, certificate not saved");
						NVMC_LOG("Failed to open ", save_name, " for writing, certificate not saved: ", error.what())
						return;
					}
					
					vmime::utility::outputStreamAdapter osa(save_file);
					first->write(osa, response == "der" ? vmime::security::cert::X509Certificate::Format::FORMAT_DER : vmime::security::cert::X509Certificate::Format::FORMAT_PEM);

					NVMC_LOG("Saved subject's certificate as ", save_name)
				}
			}
			else
			{
				rejected.push_back(first);

				vmime::security::cert::certificateException error;
				error.setCertificate(first);
				throw error;
			}
		}
	}
}

inline std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>> Nvmc::Cert::Verifier::Interactive::load_certificates(const std::filesystem::path& certs_dir) const
{
	std::unique_ptr<Nvmc::Cert::Loader::Basic> loader(std::make_unique<Nvmc::Cert::Loader::Basic>());

	try
	{
		for (const auto& entry : std::filesystem::directory_iterator(certs_dir))
		{
			try
			{
				loader->add(entry);
			}
			catch (const std::filesystem::filesystem_error& error)
			{
				NVMC_LOG("Failed to open certificate file ", entry, ": ", error.what())
				Nvmc::Aux::Msg::warn("Failed to open certificate file ", entry);
			}
		}
	}
	catch (const std::filesystem::filesystem_error& error)
	{
		NVMC_LOG("Failed to open certificate directory ", certs_dir, ": ", error.what());
	}

	return loader->get();
}
