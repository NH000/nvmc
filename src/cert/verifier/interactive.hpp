#ifndef NVMC_CERT_VERIFIER_INTERACTIVE_HPP
#define NVMC_CERT_VERIFIER_INTERACTIVE_HPP
#include <vmime/vmime.hpp>
#include <filesystem>
#include <vector>

namespace Nvmc
{
	namespace Cert
	{
		namespace Verifier
		{
			class Interactive : public vmime::security::cert::certificateVerifier
			{
				public:
					explicit Interactive(std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>&& certs_root, std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>>&& certs_user);
					explicit Interactive(const std::filesystem::path& certs_root_dir, const std::filesystem::path& certs_user_dir);

					void verify(const std::shared_ptr<vmime::security::cert::certificateChain>& chain, const std::string& hostname) override;

				protected:
					const std::filesystem::path certs_root_dir, certs_user_dir;
					std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>> certs_root, certs_user;

				private:
					std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>> rejected;

					void verifyX509(const std::shared_ptr<vmime::security::cert::certificateChain>& chain, const std::string& hostname);
					inline std::vector<std::shared_ptr<vmime::security::cert::X509Certificate>> load_certificates(const std::filesystem::path& certs_dir) const;
			};
		}
	}
}

#endif
