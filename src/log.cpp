/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "log.hpp"
#include "exception.hpp"

Nvmc::Log::Log(const char *filename)
{
	if (filename != nullptr)
	{
		file.exceptions(std::ios_base::badbit);

		try
		{
			file.open(filename, std::ios::trunc);
		}
		catch (const std::ios_base::failure& error)
		{
			throw Nvmc::Exception::Filesystem::FailureOpen(filename, false, true, error.code());
		}
	}
}

Nvmc::Log& Nvmc::Log::instance()
{
	static Nvmc::Log object(getenv("NVMC_LOG_FILE"));
	return object;
}
