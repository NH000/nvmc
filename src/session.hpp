/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_SESSION_HPP
#define NVMC_SESSION_HPP
#include <vmime/vmime.hpp>
#include <filesystem>
#include "options.hpp"

namespace Nvmc
{
	class Session
	{
		public:
			explicit Session();
			explicit Session(const Options& options);
			~Session();

			void set_tls_properties(const vmime::net::tls::TLSProperties::GenericCipherSuite strength);

			template <typename T>
			void set_certificate_verifier(const vmime::net::service::Type type, T&& verifier);

			bool is_active(const vmime::net::service::Type type) const noexcept;
			bool is_connected(const vmime::net::service::Type type) const;

			void activate(const vmime::net::service::Type type, const Options& options);
			void deactivate(const vmime::net::service::Type type);

			void connect(const vmime::net::service::Type type);
			void disconnect(const vmime::net::service::Type type);

		protected:
			std::shared_ptr<vmime::net::session> session;

			std::shared_ptr<vmime::net::store> service_store;
			std::shared_ptr<vmime::net::transport> service_transport;

		private:
			inline std::shared_ptr<vmime::security::authenticator> create_authenticator(bool sasl, const std::string& username, const std::string& password) const;
	};
}

#include "session.tpp"

#endif
