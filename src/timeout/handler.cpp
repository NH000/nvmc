#include "handler.hpp"
#include "../prompt/factory.hpp"

Nvmc::Timeout::Handler::Handler(std::time_t limit)
	: limit(limit),
	start_time(std::time(nullptr))
{
}

bool Nvmc::Timeout::Handler::isTimeOut()
{
	return limit < 0 ? false : std::time(nullptr) >= start_time + limit;
}

void Nvmc::Timeout::Handler::resetTimeOut()
{
	start_time = std::time(nullptr);
}

bool Nvmc::Timeout::Handler::handleTimeOut()
{
	auto prompt = Nvmc::Prompt::Factory::create();
	prompt->set_question("Operation timed out. Retry?");
	prompt->add_answer("yes", "y");
	prompt->add_answer("no", "n");
	return prompt->run() == "yes";
}
