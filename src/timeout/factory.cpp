#include "factory.hpp"
#include "handler.hpp"
#include "../log.hpp"

Nvmc::Timeout::Factory::Factory(std::time_t limit)
	: limit(limit)
{
	NVMC_LOG("Timeout set to ", limit)
}

std::shared_ptr<vmime::net::timeoutHandler> Nvmc::Timeout::Factory::create()
{
	return std::make_shared<Nvmc::Timeout::Handler>(limit);
}

