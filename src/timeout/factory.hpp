/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_TIMEOUT_FACTORY_HPP
#define NVMC_TIMEOUT_FACTORY_HPP
#include <vmime/vmime.hpp>
#include <ctime>

namespace Nvmc
{
	namespace Timeout
	{
		class Factory : public vmime::net::timeoutHandlerFactory
		{
			public:
				explicit Factory(std::time_t limit);
				std::shared_ptr<vmime::net::timeoutHandler> create() override;

			private:
				const std::time_t limit;
		};
	}
}

#endif
