#include <utility>
#include <iostream>
#include "info.hpp"

template <typename... T, typename>
void Nvmc::Aux::Msg::info(T&&... msgs)
{
	(std::cout << ... << std::forward<T>(msgs)) << std::endl;
}

template <typename... T, typename>
void Nvmc::Aux::Msg::error(T&&... msgs)
{
	std::cerr << NVMC_INFO_NAME " error: ";
	(std::cerr << ... << std::forward<T>(msgs)) << std::endl;
}

template <typename... T, typename>
void Nvmc::Aux::Msg::warn(T&&... msgs)
{
	std::cerr << NVMC_INFO_NAME " warning: ";
	(std::cerr << ... << std::forward<T>(msgs)) << std::endl;
}

template <typename... T>
void Nvmc::Aux::Info::help(T&&... descs)
{
	std::cout << NVMC_INFO_NAME " - " NVMC_INFO_DESC << std::endl << std::endl;

	for (const auto& desc : {descs...})
		std::cout << desc << std::endl;

	std::cout << "For more information on the program see " NVMC_INFO_NAME "(1)." << std::endl;
	std::cout << "Details on the configuration file can be found in " NVMC_INFO_NAME "(5)." << std::endl;
}
