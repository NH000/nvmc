/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_AUTHENTICATOR_SASL_HPP
#define NVMC_AUTHENTICATOR_SASL_HPP
#include <vmime/vmime.hpp>

namespace Nvmc
{
	namespace Authenticator
	{
		class SASL : public vmime::security::sasl::defaultSASLAuthenticator
		{
			public:
				explicit SASL(const std::string& username, const std::string& password);

				const std::string getUsername() const override;
				const std::string getPassword() const override;

			private:
				std::string username;
				std::string password;
		};
	}
}

#endif
