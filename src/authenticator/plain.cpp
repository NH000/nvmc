/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "plain.hpp"
#include "../log.hpp"

Nvmc::Authenticator::Plain::Plain(const std::string& username, const std::string& password)
	: username(username),
	password(password)
{
	NVMC_LOG("Username set to \"", username, "\" and password set to \"", password, "\"")
}

const std::string Nvmc::Authenticator::Plain::getUsername() const
{
	return username;
}

const std::string Nvmc::Authenticator::Plain::getPassword() const
{
	return password;
}
