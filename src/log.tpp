#include <utility>
#include "aux.hpp"

template <typename T, typename... U, typename>
void Nvmc::Log::write(T&& func, U&&... msgs)
{
	if (file.is_open())
	{
#ifdef NVMC_LOG_PREFIX
		file << "==" << Nvmc::Aux::date_and_time() << "== " << std::forward<T>(func) << ": ";
#endif

		(file << ... << std::forward<U>(msgs)) << std::endl;
	}
}
