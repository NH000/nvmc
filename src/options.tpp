template <typename T>
const T& Nvmc::Options::get_value(const std::string& name) const
{
	return option_map[name].as<T>();
}
