/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include "options.hpp"
#include "validator/timeout.hpp"
#include "validator/protocol.hpp"
#include "validator/tls_cipher_strength.hpp"
#include "aux.hpp"
#include "exception.hpp"
#include "log.hpp"

Nvmc::Options::Options()
	: desc_generic("Generic options"),
	desc_config("Configuration options"),
	desc_env("Environment variables")
{
}

void Nvmc::Options::parse_args(const int& argc, const char * const *argv)
{
	if (argc < 1 || argv == nullptr)
		throw Nvmc::Exception::CorruptedCmdArgs();

	NVMC_LOG("Parsing command-line arguments")

	init_desc_generic();

	boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc_generic), option_map);
}

void Nvmc::Options::parse_config(const std::filesystem::path& filename)
{
	if (!std::filesystem::is_regular_file(filename))
		return;

	NVMC_LOG("Parsing configuration file ", filename)

	init_desc_config();

	boost::program_options::store(boost::program_options::parse_config_file(filename.c_str(), desc_config), option_map);
}

void Nvmc::Options::parse_env()
{
	NVMC_LOG("Parsing environment")

	init_desc_env();

	boost::program_options::store(boost::program_options::parse_environment(desc_env,	[](const std::string& name) -> std::string
																						{
																							if (name == "USER")
																								return "user";
																							if (name == "EDITOR")
																								return "editor";

																							return "";
																						}), option_map);
}

void Nvmc::Options::parse_done()
{
	NVMC_LOG("Done with parsing, setting rest of the options to default values")

	boost::program_options::options_description desc_default;

	desc_default.add_options()
		("name", boost::program_options::value<std::string>()->default_value(""))
		("editor", boost::program_options::value<std::string>()->default_value("nano"))
		("store.use", boost::program_options::value<bool>()->default_value(false))
		("store.timeout", boost::program_options::value<Nvmc::Validator::Timeout>()->default_value(Nvmc::Validator::Timeout(-1)))
		("store.protocol", boost::program_options::value<Nvmc::Validator::ProtocolStore>()->default_value(Nvmc::Validator::ProtocolStore("maildir")))
		("store.protocol.imap.sasl", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.imap.tls", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.imap.auth.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.imap.auth.password", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.imap.server.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.imap.server.port", boost::program_options::value<unsigned int>()->default_value(145))
		("store.protocol.imaps.sasl", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.imaps.auth.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.imaps.auth.password", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.imaps.server.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.imaps.server.port", boost::program_options::value<unsigned int>()->default_value(993))
		("store.protocol.pop3.sasl", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.pop3.apop", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.pop3.tls", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.pop3.auth.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.pop3.auth.password", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.pop3.server.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.pop3.server.port", boost::program_options::value<unsigned int>()->default_value(110))
		("store.protocol.pop3s.sasl", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.pop3s.apop", boost::program_options::value<bool>()->default_value(false))
		("store.protocol.pop3s.auth.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.pop3s.auth.password", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.pop3s.server.address", boost::program_options::value<std::string>()->default_value(""))
		("store.protocol.pop3s.server.port", boost::program_options::value<unsigned int>()->default_value(995))
		("store.protocol.maildir.folder", boost::program_options::value<std::filesystem::path>()->default_value(Nvmc::Aux::Dir::home() / "Maildir/"))
		("transport.use", boost::program_options::value<bool>()->default_value(false))
		("transport.timeout", boost::program_options::value<Nvmc::Validator::Timeout>()->default_value(Nvmc::Validator::Timeout(-1)))
		("transport.protocol", boost::program_options::value<Nvmc::Validator::ProtocolTransport>()->default_value(Nvmc::Validator::ProtocolTransport("sendmail")))
		("transport.protocol.smtp.sasl", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtp.tls", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtp.auth.address", boost::program_options::value<std::string>()->default_value(""))
		("transport.protocol.smtp.auth.password", boost::program_options::value<std::string>()->default_value(""))
		("transport.protocol.smtp.server.address", boost::program_options::value<std::string>()->default_value(""))
		("transport.protocol.smtp.server.port", boost::program_options::value<unsigned int>()->default_value(25))
		("transport.protocol.smtp.server.authentication", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtp.server.pipelining", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtp.server.chunking", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtps.sasl", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtps.auth.address", boost::program_options::value<std::string>()->default_value(""))
		("transport.protocol.smtps.auth.password", boost::program_options::value<std::string>()->default_value(""))
		("transport.protocol.smtps.server.address", boost::program_options::value<std::string>()->default_value(""))
		("transport.protocol.smtps.server.port", boost::program_options::value<unsigned int>()->default_value(465))
		("transport.protocol.smtps.server.authentication", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtps.server.pipelining", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.smtps.server.chunking", boost::program_options::value<bool>()->default_value(false))
		("transport.protocol.sendmail.bin", boost::program_options::value<std::filesystem::path>()->default_value("/usr/bin/sendmail"))
		("tls.cipher_strength", boost::program_options::value<Nvmc::Validator::TLSCipherStrength>()->default_value(Nvmc::Validator::TLSCipherStrength("default")))
		("tls.certs.root", boost::program_options::value<std::filesystem::path>()->default_value(Nvmc::Aux::default_certs_root()))
		("tls.certs.user", boost::program_options::value<std::filesystem::path>()->default_value(Nvmc::Aux::default_certs_user()));

		std::istringstream iss;
		boost::program_options::store(boost::program_options::parse_config_file(iss, desc_default), option_map);

		NVMC_LOG("Running option notifiers")
		boost::program_options::notify(option_map);
}

const boost::program_options::options_description& Nvmc::Options::get_desc_generic() const
{
	init_desc_generic();
	return desc_generic;
}

const boost::program_options::options_description& Nvmc::Options::get_desc_config() const
{
	init_desc_config();
	return desc_config;
}

const boost::program_options::options_description& Nvmc::Options::get_desc_env() const
{
	init_desc_env();
	return desc_env;
}

template <>
const bool& Nvmc::Options::get_value<bool>(const std::string& name) const
{
	try
	{
		return option_map[name].as<bool>();
	}
	catch (const boost::wrapexcept<boost::bad_any_cast>& error)
	{
		static bool count;
		count = option_map.count(name);
		return count;
	}
}

void Nvmc::Options::init_desc_generic() const
{
	static bool init = false;
	if (!init)
	{
		init = true;

		desc_generic.add_options()
			("help", "show help menu")
			("version", "show version")
			("license", "show license");
	}
}

void Nvmc::Options::init_desc_config() const
{
	static bool init = false;
	if (!init)
	{
		init = true;

		desc_config.add_options()
			("name", boost::program_options::value<std::string>()->value_name("STRING"), "name of the user")
			("editor", boost::program_options::value<std::string>()->value_name("STRING"), "program to use for editing mails")
			("store.use", boost::program_options::value<bool>()->value_name("BOOL"), "use store service")
			("store.timeout", boost::program_options::value<Nvmc::Validator::Timeout>()->value_name("INT"), "timeout limit for service operations (negative to disable)")
			("store.protocol", boost::program_options::value<Nvmc::Validator::ProtocolStore>()->value_name("PROTOCOL"), "store protocol: imap, imaps, pop3, pop3s, maildir")
			("store.protocol.imap.sasl", boost::program_options::value<bool>()->value_name("BOOL"), "use SASL for authentication")
			("store.protocol.imap.tls", boost::program_options::value<bool>()->value_name("BOOL"), "attempt to establish TLS connection with the server")
			("store.protocol.imap.auth.address", boost::program_options::value<std::string>()->value_name("EMAIL"), "your email address")
			("store.protocol.imap.auth.password", boost::program_options::value<std::string>()->value_name("STRING"), "your password")
			("store.protocol.imap.server.address", boost::program_options::value<std::string>()->value_name("ADDRESS"), "server to connect to")
			("store.protocol.imap.server.port", boost::program_options::value<unsigned int>()->value_name("UINT"), "port to connect on")
			("store.protocol.imaps.sasl", boost::program_options::value<bool>()->value_name("BOOL"), "use SASL for authentication")
			("store.protocol.imaps.auth.address", boost::program_options::value<std::string>()->value_name("EMAIL"), "your email address")
			("store.protocol.imaps.auth.password", boost::program_options::value<std::string>()->value_name("STRING"), "your password")
			("store.protocol.imaps.server.address", boost::program_options::value<std::string>()->value_name("ADDRESS"), "servert to connect to")
			("store.protocol.imaps.server.port", boost::program_options::value<unsigned int>()->value_name("UINT"), "port to connect on")
			("store.protocol.pop3.sasl", boost::program_options::value<bool>()->value_name("BOOL"), "use SASL for authentication")
			("store.protocol.pop3.apop", boost::program_options::value<bool>()->value_name("BOOL"), "use APOP for authentication")
			("store.protocol.pop3.tls", boost::program_options::value<bool>()->value_name("BOOL"), "attempt to establish TLS connection with the server")
			("store.protocol.pop3.auth.address", boost::program_options::value<std::string>()->value_name("EMAIL"), "your email address")
			("store.protocol.pop3.auth.password", boost::program_options::value<std::string>()->value_name("STRING"), "your password")
			("store.protocol.pop3.server.address", boost::program_options::value<std::string>()->value_name("ADDRESS"), "server to connect to")
			("store.protocol.pop3.server.port", boost::program_options::value<unsigned int>()->value_name("UINT"), "port to connect on")
			("store.protocol.pop3s.sasl", boost::program_options::value<bool>()->value_name("BOOL"), "use SASL for authentication")
			("store.protocol.pop3s.apop", boost::program_options::value<bool>()->value_name("BOOL"), "use APOP for authentication")
			("store.protocol.pop3s.auth.address", boost::program_options::value<std::string>()->value_name("EMAIL"), "your email address")
			("store.protocol.pop3s.auth.password", boost::program_options::value<std::string>()->value_name("STRING"), "your password")
			("store.protocol.pop3s.server.address", boost::program_options::value<std::string>()->value_name("ADDRESS"), "server to connect to")
			("store.protocol.pop3s.server.port", boost::program_options::value<unsigned int>()->value_name("UINT"), "port to connect on")
			("store.protocol.maildir.folder", boost::program_options::value<std::filesystem::path>()->value_name("PATH"), "path to the local maildir")
			("transport.use", boost::program_options::value<bool>()->value_name("BOOL"), "use transport service")
			("transport.timeout", boost::program_options::value<Nvmc::Validator::Timeout>()->value_name("INT"), "timeout limit for service operations (negative to disable)")
			("transport.protocol", boost::program_options::value<Nvmc::Validator::ProtocolTransport>()->value_name("PROTOCOL"), "transport protocol: smtp, smtps, sendmail")
			("transport.protocol.smtp.sasl", boost::program_options::value<bool>()->value_name("BOOL"), "use SASL for authentication")
			("transport.protocol.smtp.tls", boost::program_options::value<bool>()->value_name("BOOL"), "attempt to establish TLS connection with the server")
			("transport.protocol.smtp.auth.address", boost::program_options::value<std::string>()->value_name("EMAIL"), "your email address")
			("transport.protocol.smtp.auth.password", boost::program_options::value<std::string>()->value_name("STRING"), "your password")
			("transport.protocol.smtp.server.address", boost::program_options::value<std::string>()->value_name("ADDRESS"), "server to connect to")
			("transport.protocol.smtp.server.port", boost::program_options::value<unsigned int>()->value_name("UINT"), "port to connect on")
			("transport.protocol.smtp.server.authentication", boost::program_options::value<bool>()->value_name("BOOL"), "is authentication to the server ?")
			("transport.protocol.smtp.server.pipelining", boost::program_options::value<bool>()->value_name("BOOL"), "set to false to disable command pipelining (if the server supports it)")
			("transport.protocol.smtp.server.chunking", boost::program_options::value<bool>()->value_name("BOOL"), "set to false to disable chunking extension (if the server supports)")
			("transport.protocol.smtps.sasl", boost::program_options::value<bool>()->value_name("BOOL"), "use SASL for authentication")
			("transport.protocol.smtps.auth.address", boost::program_options::value<std::string>()->value_name("EMAIL"), "your email address")
			("transport.protocol.smtps.auth.password", boost::program_options::value<std::string>()->value_name("STRING"), "your password")
			("transport.protocol.smtps.server.address", boost::program_options::value<std::string>()->value_name("ADDRESS"), "server to connect to")
			("transport.protocol.smtps.server.port", boost::program_options::value<unsigned int>()->value_name("UINT"), "port to connect on")
			("transport.protocol.smtps.server.authentication", boost::program_options::value<bool>()->value_name("BOOL"), "is authentication to the server ?")
			("transport.protocol.smtps.server.pipelining", boost::program_options::value<bool>()->value_name("BOOL"), "set to false to disable command pipelining (if the server supports it)")
			("transport.protocol.smtps.server.chunking", boost::program_options::value<bool>()->value_name("BOOL"), "set to false to disable chunking extension (if the server supports it)")
			("transport.protocol.sendmail.bin", boost::program_options::value<std::filesystem::path>()->value_name("PATH"), "path to the sendmail executable")
			("tls.cipher_strength", boost::program_options::value<Nvmc::Validator::TLSCipherStrength>()->value_name("STRENGTH"), "strength level of the TLS cipher")
			("tls.certs.root", boost::program_options::value<std::filesystem::path>()->value_name("PATH"), "directory with root certificates")
			("tls.certs.user", boost::program_options::value<std::filesystem::path>()->value_name("PATH"), "directory with user certificates");
	}
}

void Nvmc::Options::init_desc_env() const
{
	static bool init = false;
	if (!init)
	{
		init = true;

		desc_env.add_options()
			("user", boost::program_options::value<std::string>()->value_name("STRING"), "name of the user")
			("editor", boost::program_options::value<std::string>()->value_name("STRING"), "program to use for editing mails");
	}
}
