/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include "exception.hpp"

const char *Nvmc::Exception::DateAndTimeFailure::what() const noexcept
{
	return "Failed to get date and time";
}

const char *Nvmc::Exception::HomeUnavailable::what() const noexcept
{
	return "Unable to determine location of the home directory";
}

const char *Nvmc::Exception::CorruptedCmdArgs::what() const noexcept
{
	return "Corrupted command-line argument data (argc < 1 || argv == nullptr)";
}

Nvmc::Exception::Filesystem::FailureOpen::FailureOpen(const std::filesystem::path& filename, bool read, bool write, const std::error_code& error_code)
	: std::filesystem::filesystem_error(std::string("Failed to open file [") + (read ? "r" : "") + (write ? "w" : "") + "]", filename, error_code)
{
}

Nvmc::Exception::Service::Active::Active(vmime::net::service::Type type)
	: msg(std::string(type == vmime::net::service::Type::TYPE_STORE ? "Store" : "Transport") + " service is already active")
{
}

const char *Nvmc::Exception::Service::Active::what() const noexcept
{
	return msg.c_str();
}

Nvmc::Exception::Service::Inactive::Inactive(vmime::net::service::Type type)
	: msg(std::string(type == vmime::net::service::Type::TYPE_STORE ? "Store" : "Transport") + " service is not active")
{
}

const char *Nvmc::Exception::Service::Inactive::what() const noexcept
{
	return msg.c_str();
}

Nvmc::Exception::Service::Connected::Connected(vmime::net::service::Type type)
	: msg(std::string(type == vmime::net::service::Type::TYPE_STORE ? "Store" : "Transport") + " service is already connected")
{
}

const char *Nvmc::Exception::Service::Connected::what() const noexcept
{
	return msg.c_str();
}

Nvmc::Exception::Service::Disconnected::Disconnected(vmime::net::service::Type type)
	: msg(std::string(type == vmime::net::service::Type::TYPE_STORE ? "Store" : "Transport") + " service is not connected")
{
}

const char *Nvmc::Exception::Service::Disconnected::what() const noexcept
{
	return msg.c_str();
}
