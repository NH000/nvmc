/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_VALIDATOR_PROTOCOL_HPP
#define NVMC_VALIDATOR_PROTOCOL_HPP
#include <boost/any.hpp>
#include <vmime/vmime.hpp>
#include <vector>
#include <string>

namespace Nvmc
{
	namespace Validator
	{
		class Protocol
		{
			friend std::istream& operator>>(std::istream& is, Protocol& protocol);
			friend std::ostream& operator<<(std::ostream& os, const Protocol& protocol);

			public:
				virtual ~Protocol() = default;

				operator const std::string&() const noexcept;
				virtual operator vmime::net::service::Type() const noexcept = 0;

			protected:
				explicit Protocol(const std::string& protocol);

				std::string protocol;
		};

		class ProtocolStore : public Protocol
		{
			public:
				explicit ProtocolStore() = default;
				explicit ProtocolStore(const std::string& protocol);

				operator vmime::net::service::Type() const noexcept override;
		};

		class ProtocolTransport : public Protocol
		{
			public:
				explicit ProtocolTransport() = default;
				explicit ProtocolTransport(const std::string& protocol);

				operator vmime::net::service::Type() const noexcept override;
		};

		std::istream& operator>>(std::istream& is, Protocol& protocol);
		std::ostream& operator<<(std::ostream& os, const Protocol& protocol);

		void validate(boost::any& v, std::vector<std::string> values, ProtocolStore *, int);
		void validate(boost::any& v, std::vector<std::string> values, ProtocolTransport *, int);
	}
}

#endif

