#include <stdexcept>
#include <boost/program_options/value_semantic.hpp>
#include "tls_cipher_strength.hpp"

Nvmc::Validator::TLSCipherStrength::TLSCipherStrength(const std::string& strength)
	: strength(strength)
{
	if (strength != "default" && strength != "low" && strength != "medium" && strength != "high")
		throw std::invalid_argument("strength");
}

Nvmc::Validator::TLSCipherStrength::operator const std::string&() const noexcept
{
	return strength;
}

Nvmc::Validator::TLSCipherStrength::operator vmime::net::tls::TLSProperties::GenericCipherSuite() const noexcept
{
	if (strength == "low")
		return vmime::net::tls::TLSProperties::GenericCipherSuite::CIPHERSUITE_LOW;
	else if (strength == "medium")
		return vmime::net::tls::TLSProperties::GenericCipherSuite::CIPHERSUITE_MEDIUM;
	else if (strength == "high")
		return vmime::net::tls::TLSProperties::GenericCipherSuite::CIPHERSUITE_HIGH;

	return vmime::net::tls::TLSProperties::GenericCipherSuite::CIPHERSUITE_DEFAULT;
}

std::istream& Nvmc::Validator::operator>>(std::istream& is, Nvmc::Validator::TLSCipherStrength& strength)
{
	return is >> strength.strength;
}

std::ostream& Nvmc::Validator::operator<<(std::ostream& os, const Nvmc::Validator::TLSCipherStrength& strength)
{
	return os << strength.strength;
}

void Nvmc::Validator::validate(boost::any& v, std::vector<std::string>& values, Nvmc::Validator::TLSCipherStrength *, int)
{
	boost::program_options::validators::check_first_occurrence(v);
	const auto& s = boost::program_options::validators::get_single_string(values);

	try
	{
		v = boost::any(Nvmc::Validator::TLSCipherStrength(s));
	}
	catch (const std::invalid_argument& error)
	{
		throw boost::program_options::validation_error::invalid_option_value;

	}
}
