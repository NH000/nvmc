#include <stdexcept>
#include <boost/program_options/value_semantic.hpp>
#include <climits>
#include "timeout.hpp"

#define NVMC_VALIDATOR_TIMEOUT_LIMIT_MIN	SCHAR_MIN
#define NVMC_VALIDATOR_TIMEOUT_LIMIT_MAX	300

Nvmc::Validator::Timeout::Timeout(std::time_t limit)
	: limit(limit)
{
	if (!(limit >= NVMC_VALIDATOR_TIMEOUT_LIMIT_MIN && limit <= NVMC_VALIDATOR_TIMEOUT_LIMIT_MAX))
		throw std::invalid_argument("limit");
}

Nvmc::Validator::Timeout::operator std::time_t() const noexcept
{
	return limit;
}

std::istream& Nvmc::Validator::operator>>(std::istream& is, Nvmc::Validator::Timeout& timeout)
{
	return is >> timeout.limit;
}

std::ostream& Nvmc::Validator::operator<<(std::ostream& os, const Nvmc::Validator::Timeout& timeout)
{
	return os << timeout.limit;
}

void Nvmc::Validator::validate(boost::any& v, std::vector<std::string> values, Nvmc::Validator::Timeout *, int)
{
	boost::program_options::validators::check_first_occurrence(v);
	const auto& s = boost::program_options::validators::get_single_string(values);

	try
	{
		v = boost::any(Nvmc::Validator::Timeout(boost::lexical_cast<std::time_t>(s)));
	}
	catch (const std::invalid_argument& error)
	{
		throw boost::program_options::validation_error::invalid_option_value;
	}
}
