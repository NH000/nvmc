#include <stdexcept>
#include <boost/program_options/value_semantic.hpp>
#include "protocol.hpp"

Nvmc::Validator::Protocol::Protocol(const std::string& protocol)
	: protocol(protocol)
{
}

Nvmc::Validator::Protocol::operator const std::string&() const noexcept
{
	return protocol;
}

Nvmc::Validator::ProtocolStore::ProtocolStore(const std::string& protocol)
	: Nvmc::Validator::Protocol::Protocol(protocol)
{
	if (protocol != "imap" && protocol != "imaps" && protocol != "pop3" && protocol != "pop3s" && protocol != "maildir")
		throw std::invalid_argument(protocol);
}

Nvmc::Validator::ProtocolStore::operator vmime::net::service::Type() const noexcept
{
	return vmime::net::service::Type::TYPE_STORE;
}

Nvmc::Validator::ProtocolTransport::ProtocolTransport(const std::string& protocol)
	: Nvmc::Validator::Protocol::Protocol(protocol)
{
	if (protocol != "smtp" && protocol != "smtps" && protocol != "sendmail")
		throw std::invalid_argument(protocol);
}

Nvmc::Validator::ProtocolTransport::operator vmime::net::service::Type() const noexcept
{
	return vmime::net::service::Type::TYPE_TRANSPORT;
}

std::istream& Nvmc::Validator::operator>>(std::istream& is, Nvmc::Validator::Protocol& protocol)
{
	return is >> protocol.protocol;
}

std::ostream& Nvmc::Validator::operator<<(std::ostream& os, const Nvmc::Validator::Protocol& protocol)
{
	return os << protocol.protocol;
}

void Nvmc::Validator::validate(boost::any& v, std::vector<std::string> values, Nvmc::Validator::ProtocolStore *, int)
{
	boost::program_options::validators::check_first_occurrence(v);
	const auto& s = boost::program_options::validators::get_single_string(values);

	try
	{
		v = boost::any(Nvmc::Validator::ProtocolStore(s));
	}
	catch (const std::invalid_argument& error)
	{
		throw boost::program_options::validation_error::invalid_option_value;

	}
}

void Nvmc::Validator::validate(boost::any& v, std::vector<std::string> values, Nvmc::Validator::ProtocolTransport *, int)
{
	boost::program_options::validators::check_first_occurrence(v);
	const auto& s = boost::program_options::validators::get_single_string(values);

	try
	{
		v = boost::any(Nvmc::Validator::ProtocolTransport(s));
	}
	catch (const std::invalid_argument& error)
	{
		throw boost::program_options::validation_error::invalid_option_value;

	}
}
