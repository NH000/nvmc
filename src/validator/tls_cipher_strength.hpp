/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_VALIDATOR_TLS_CIPHER_STRENGTH_HPP
#define NVMC_VALIDATOR_TLS_CIPHER_STRENGTH_HPP
#include <vmime/vmime.hpp>
#include <boost/any.hpp>
#include <string>
#include <vector>

namespace Nvmc
{
	namespace Validator
	{
		class TLSCipherStrength
		{
			friend std::istream& operator>>(std::istream& is, TLSCipherStrength& strength);
			friend std::ostream& operator<<(std::ostream& os, const TLSCipherStrength& strength);

			public:
				explicit TLSCipherStrength() = default;
				explicit TLSCipherStrength(const std::string& strength);

				operator const std::string&() const noexcept;
				operator vmime::net::tls::TLSProperties::GenericCipherSuite() const noexcept;

			protected:
				std::string strength;
		};

		void validate(boost::any& v, std::vector<std::string>& values, TLSCipherStrength *, int);

		std::istream& operator>>(std::istream& is, TLSCipherStrength& strength);
		std::ostream& operator<<(std::ostream& os, const TLSCipherStrength& strength);
	}
}

#endif
