/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_VALIDATOR_TIMEOUT_HPP
#define NVMC_VALIDATOR_TIMEOUT_HPP
#include <boost/any.hpp>
#include <vector>
#include <string>
#include <ctime>

namespace Nvmc
{
	namespace Validator
	{
		class Timeout
		{
			friend std::istream& operator>>(std::istream& is, Timeout& timeout);
			friend std::ostream& operator<<(std::ostream& os, const Timeout& timeout);

			public:
				explicit Timeout() = default;
				explicit Timeout(std::time_t limit);

				operator std::time_t() const noexcept;

			protected:
				std::time_t limit;
		};

		std::istream& operator>>(std::istream& is, Timeout& timeout);
		std::ostream& operator<<(std::ostream& os, const Timeout& timeout);

		void validate(boost::any& v, std::vector<std::string> values, Timeout *, int);
	}
}

#endif
