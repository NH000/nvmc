/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NVMC_SUCCESS_CODE_HPP
#define NVMC_SUCCESS_CODE_HPP
#include <stdexcept>

#define NVMC_EXIT_MSG(code)	"Exiting with success code ", Nvmc::SuccessCode::name(code)

namespace Nvmc
{
	namespace SuccessCode
	{
		enum Type
		{
			SUCCESS,
			INVALID_OPT
		};

		static inline const char *name(Type type)
		{
			if (type == Type::SUCCESS)
				return "SUCCESS";
			else if (type == Type::INVALID_OPT)
				return "INVALID_OPT";

			throw std::invalid_argument("type");
		}
	}
}

#endif
