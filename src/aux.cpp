/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of NVMC.
 *
 * NVMC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NVMC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NVMC.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <iostream>
#include <csignal>
#include <ctime>
#include <clocale>
#include "exception.hpp"
#include "aux.hpp"
#include "info.hpp"

extern "C"
{
#include <pwd.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
}

std::mutex Nvmc::Aux::Lock::set_EOF;

void Nvmc::Aux::set_EOF(bool enable)
{
	if (!isatty(STDOUT_FILENO))
		return;

	struct termios attr;
	tcgetattr(STDOUT_FILENO, &attr);

	if (enable)
	{
		signal(SIGCONT, SIG_DFL);
		std::set_terminate(nullptr);

		attr.c_cc[VEOF] = CEOF;
	}
	else
	{
		signal(SIGCONT,	[](int) -> void
						{
							if (Nvmc::Aux::Lock::set_EOF.try_lock())
							{
								Nvmc::Aux::set_EOF(false);
								Nvmc::Aux::Lock::set_EOF.unlock();
							}
						});
		std::set_terminate(	[]() -> void
							{
								Nvmc::Aux::Lock::set_EOF.lock();
								Nvmc::Aux::set_EOF(true);
								std::abort();
							});

		attr.c_cc[VEOF] = _POSIX_VDISABLE;
	}

	tcsetattr(STDOUT_FILENO, TCSANOW, &attr);
}

std::string Nvmc::Aux::date_and_time()
{
	char time_str[20];
	const auto time = std::time(nullptr);

	if (!std::strftime(time_str, sizeof(time_str), "%F %T", std::localtime(&time)))	// See strftime(3).
		throw Nvmc::Exception::DateAndTimeFailure();

	return time_str;
}

std::filesystem::path Nvmc::Aux::config_file(bool global)
{
	#define NVMC_CONFIG_FILE_DEFAULT_NAME	NVMC_INFO_NAME ".ini"

	if (global)
		return "/etc/" NVMC_CONFIG_FILE_DEFAULT_NAME;

	const auto *config = getenv("NVMC_CONFIG");
	return config ? config : Nvmc::Aux::Dir::app_config() / NVMC_CONFIG_FILE_DEFAULT_NAME;

	#undef NVMC_CONFIG_FILE_DEFAULT_NAME
}

std::filesystem::path Nvmc::Aux::default_certs_root()
{
	return Nvmc::Aux::Dir::app_data() / "certs/root/";
}

std::filesystem::path Nvmc::Aux::default_certs_user()
{
	return Nvmc::Aux::Dir::app_data() / "certs/user/";
}

std::filesystem::path Nvmc::Aux::Dir::home()
{
	const auto *home = getenv("HOME");

	if (!home)
	{
		const auto *pw = getpwuid(getuid());
		if (!pw)
			throw Nvmc::Exception::HomeUnavailable();

		home = pw->pw_dir;
	}

	return home;
}

std::filesystem::path Nvmc::Aux::Dir::config()
{
	const auto *dir = getenv("XDG_CONFIG_HOME");
	return dir ? dir : Nvmc::Aux::Dir::home() / ".config/";
}

std::filesystem::path Nvmc::Aux::Dir::data()
{
	const auto *dir = getenv("XDG_DATA_HOME");
	return dir ? dir : Nvmc::Aux::Dir::home() / ".local/share/";
}

std::filesystem::path Nvmc::Aux::Dir::cache()
{
	const auto *dir = getenv("XDG_CACHE_HOME");
	return dir ? dir : Nvmc::Aux::Dir::home() / ".cache/";
}

std::filesystem::path Nvmc::Aux::Dir::app_config()
{
	return Nvmc::Aux::Dir::config() / NVMC_INFO_NAME "/";
}

std::filesystem::path Nvmc::Aux::Dir::app_data()
{
	return Nvmc::Aux::Dir::data() / NVMC_INFO_NAME "/";
}

std::filesystem::path Nvmc::Aux::Dir::app_cache()
{
	return Nvmc::Aux::Dir::cache() / NVMC_INFO_NAME "/";
}

void Nvmc::Aux::Info::version()
{
	std::cout << NVMC_INFO_VERS << std::endl;
}

void Nvmc::Aux::Info::license()
{
	std::cout << NVMC_INFO_LICE << std::endl;
}
